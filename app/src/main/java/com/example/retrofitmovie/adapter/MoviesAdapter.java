package com.example.retrofitmovie.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.RecyclerView;

import com.example.retrofitmovie.R;
import com.example.retrofitmovie.activity.MainActivity;
import com.example.retrofitmovie.databinding.ListItemMovieBinding;
import com.example.retrofitmovie.model.Movie;
import com.squareup.picasso.Picasso;

import java.util.List;


public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MovieViewHolder> {
    private List<Movie> movies;
    private final LayoutInflater mInflater;
    private Context context;
    public static final String IMAGE_URL_BASE_PATH ="https://image.tmdb.org/t/p/w342//";

    public MoviesAdapter(Context context) {

        mInflater = LayoutInflater.from(context);
        this.context=context;
    }

    //A view holder inner class where we get reference to the views in the layout using their ID
    public static class MovieViewHolder extends RecyclerView.ViewHolder {
    private final ListItemMovieBinding binding;
        TextView movieTitle;
        TextView data;
        TextView movieDescription;
        ImageView movieImage;

        public MovieViewHolder(ListItemMovieBinding binding) {
            super(binding.getRoot());
            this.binding=binding;

            //moviesLayout = (LinearLayout) v.findViewById(R.id.movies_layout);
           // movieImage = (ImageView) v.findViewById(R.id.movie_image);
            //movieTitle = (TextView) v.findViewById(R.id.title);
            //data = (TextView) v.findViewById(R.id.date);
            //movieDescription = (TextView) v.findViewById(R.id.description);
            //rating = (TextView) v.findViewById(R.id.rating);
        }

        public void bind(Movie movies){
            binding.setMovie(movies);

        }
    }

    @Override
    public MoviesAdapter.MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        //View view = mInflater.inflate(R.layout.list_item_movie, parent, false);
        //return new MovieViewHolder(view);

        LayoutInflater layoutInflater =
                LayoutInflater.from(parent.getContext());
        ListItemMovieBinding itemBinding =
                ListItemMovieBinding.inflate(layoutInflater, parent, false);
        return new MovieViewHolder(itemBinding);

    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, final int position) {

        Movie movies = getItemForPosition(position);
        holder.bind(movies);



/*


        if(movies!=null) {
            String image_url = IMAGE_URL_BASE_PATH + movies.get(position).getPosterPath();
            Picasso.with(context)
                   .load(image_url)
                    .placeholder(android.R.drawable.sym_def_app_icon)
                   .error(android.R.drawable.sym_def_app_icon)
                    .into(holder.movieImage);

            holder.movieTitle.setText(movies.get(position).getTitle());
            holder.data.setText(movies.get(position).getReleaseDate());
            holder.movieDescription.setText(movies.get(position).getOverview());
            //holder.rating.setText(movies.get(position).getVoteAverage().toString());
        }else {


            holder.movieTitle.setText("no movie");
            holder.data.setText("no date");
            holder.movieDescription.setText("no desc");

        }
        */
        }

    private Movie getItemForPosition(int position) {
        return movies.get(position);
    }


    @Override
    public int getItemCount() {
        int a ;

        if(movies != null && !movies.isEmpty()) {

            a = movies.size();
        }
        else {

            a = 0;

        }

        return a;
    }
    public void setMovies(List<Movie> movies1) {
        movies = movies1;
        notifyDataSetChanged();
    }

}

    

