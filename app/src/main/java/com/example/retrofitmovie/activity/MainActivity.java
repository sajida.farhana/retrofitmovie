package com.example.retrofitmovie.activity;

import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.retrofitmovie.R;
import com.example.retrofitmovie.adapter.MoviesAdapter;
import com.example.retrofitmovie.databinding.ListItemMovieBinding;
import com.example.retrofitmovie.model.Movie;
import com.example.retrofitmovie.model.MovieApiService;
import com.example.retrofitmovie.model.MovieResponse;
import com.example.retrofitmovie.model.MovieViewModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {


        private static final String TAG = MainActivity.class.getSimpleName();
        public static final String BASE_URL = "https://api.themoviedb.org/3/";

        //private static Retrofit retrofit = null;
        private RecyclerView recyclerView;
        private MoviesAdapter adapter;

        //private ListItemMovieBinding binding = DataBindingUtil.setContentView(this, R.layout.list_item_movie);

        private MovieViewModel mMovieViewModel;

        //private final static String API_KEY = "bd01fd741bd95e403da4beb3af82fd95";

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);


            recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
            recyclerView.setHasFixedSize(true);
            adapter= new MoviesAdapter(this);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(adapter);
            // newly added
            //adapter = new MoviesAdapter( movies , R.layout.list_item_movie ,getApplicationContext());
            //recyclerView.setAdapter(adapter);

            //connectAndGetApiData();

            mMovieViewModel= ViewModelProviders.of(this).get(MovieViewModel.class);



            mMovieViewModel.getMoviesLiveData().observe(this, new Observer<List<Movie>>() {
                @Override
                public void onChanged(@Nullable final List<Movie> movies1) {
                    // Update the cached copy of the movies in the adapter.
                    adapter.setMovies(movies1);
                }
            });
            mMovieViewModel.getTopRatedMovies();

           // Log.d("main act","movierecyclerView.setAdapter(adapter);
        }












/*

// This method create an instance of Retrofit
// set the base url
        public void connectAndGetApiData(){
            if (retrofit == null) {
                retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }
            MovieApiService movieApiService = retrofit.create(MovieApiService.class);
            Call<MovieResponse> call = movieApiService.getTopRatedMovies(API_KEY);
            call.enqueue(new Callback<MovieResponse>() {

                @Override
                public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                    List<Movie> movies = response.body().getResults();
                    recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
                    Log.d(TAG, "Number of movies received: " + movies.size());

                }

                @Override
                public void onFailure(Call<MovieResponse> call, Throwable throwable) {
                    Log.e(TAG, throwable.toString());
                }
            });

 */
        }


