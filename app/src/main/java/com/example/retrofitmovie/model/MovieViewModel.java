package com.example.retrofitmovie.model;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

public class MovieViewModel extends AndroidViewModel {

    private MovieRepository mMovieRepository;
    private MutableLiveData<List<Movie>> mAllMovies = new MutableLiveData();

    public MovieViewModel (Application application)
    {
        super(application);


    }

    public LiveData getMoviesLiveData() {
        return mAllMovies;
    }

    public void getTopRatedMovies()
    {
        mMovieRepository = new MovieRepository();
        mMovieRepository.getTopRatedMovies(new MovieRepository.MovieCallback() {
            @Override
            public void getResult(List<Movie> mAllMoviesInt) {
                mAllMovies.setValue(mAllMoviesInt);

            }
        });
        Log.d("viewmodel  act","movies called in view model class");

    }






}
