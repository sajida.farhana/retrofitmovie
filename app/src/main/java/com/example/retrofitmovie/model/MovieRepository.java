package com.example.retrofitmovie.model;

import android.app.Application;
import android.net.sip.SipAudioCall;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import androidx.lifecycle.LiveData;

import com.example.retrofitmovie.R;
import com.example.retrofitmovie.activity.MainActivity;
import com.example.retrofitmovie.adapter.MoviesAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

//import static com.example.retrofitmovie.activity.MainActivity.retrofit;

public class MovieRepository {

    private MovieApiService mMovieApiService;
    public List<Movie> mAllMovies;
    private static Retrofit retrofit=null;

    public static final String BASE_URL = "https://api.themoviedb.org/3/";
    private final static String API_KEY = "bd01fd741bd95e403da4beb3af82fd95";
    private static final String TAG = "movierepo class";

    public MovieRepository() {


    }

    void getTopRatedMovies(final MovieCallback callback) {

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        mMovieApiService = retrofit.create(MovieApiService.class);
        Call<MovieResponse> call = mMovieApiService.getTopRatedMovies(API_KEY);


        call.enqueue(new Callback<MovieResponse>() {

            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                mAllMovies = response.body().getResults();
                callback.getResult(mAllMovies);
                //apiCall.onSuccess(mAllMovies);
            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
                //apiCall.onFailure();
            }
        });

        //Log.d(TAG,"movies async called and executed in repo class");
        //Log.d(TAG, "Number of movies received: " + mAllMovies.size());

        //return mAllMovies;
    }







interface MovieCallback {
    void getResult(List<Movie> mAllMovies);
}


}
